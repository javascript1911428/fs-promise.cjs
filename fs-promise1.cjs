const fs = require('fs')
const path = require('path');

function createNewFile(pathOfDirectory, numberOfRandomFiles) {
    return new Promise(function(resolve, reject) {
        let pathArray = [];
        fs.mkdir(pathOfDirectory, (err) => {
            if(err) {
                reject(err)
            } else {
                let createdFiles = 0;
                for (let index = 1; index <= numberOfRandomFiles; index++) {
                    if(createdFiles < numberOfRandomFiles) {
                        let fileName = `file${index}.json`
                        const filePath = path.join(pathOfDirectory, fileName);
                        pathArray.push(filePath);
                        function randomData() {
                            let data = {
                                "even" : [2,4,6,8]
                            }
                            return JSON.stringify(data);
                        }
                        let fileData = randomData();
                        fs.writeFile(filePath, fileData, (err) => {
                            if(err) {
                                reject(err)
                            } else {
                                createdFiles += 1;
                                if(createdFiles === numberOfRandomFiles) {
                                    resolve(pathArray);
                                }

                            }
                        })
                    }
                }
            }
        }) 
    })
}


function deleteCreatedFile(pathOfDirectory, randomNumberOfFiles, pathArray) {
    return new Promise(function(resolve, reject) {
        for (let index = 1; index <= randomNumberOfFiles; index++) {
            let fileName = pathArray[index - 1];
            console.log(fileName);
            fs.unlink(fileName, (err) => {
                if(err) {
                    reject(err)
                } else {
                    if(index === pathArray.length) {
                        resolve(null)
                    }
                }
            })
        }
    })
}

let absolutePathOfRandomDirectory = path.join(__dirname, 'randomfiles');
let randomNumberOfFiles = 4;

function fsProblemPromise1(){

    createNewFile(absolutePathOfRandomDirectory, randomNumberOfFiles)
    .then((result) => {
        console.log(result);
        return deleteCreatedFile(absolutePathOfRandomDirectory, randomNumberOfFiles, result)
    })
    .then((value) => {
        console.log(value)
    })
    .catch((error) => {
        console.log(error);
    })

}

module.exports = fsProblemPromise1;
