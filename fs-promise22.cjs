function fsProblem2() {

  const fs = require('fs').promises;

  function readFileAsync(filename) {
    return fs.readFile(filename, 'utf8');
  }


  function writeFileAsync(filename, data) {
    return fs.writeFile(filename, data, 'utf8');
  }


  function convertToUpperCase(content) {
    return content.toUpperCase();
  }


  function convertToLowerCaseAndSplit(content) {
    const lowercaseContent = content.toLowerCase();
    return lowercaseContent.split(/[.!?]\s+/);
  }

  const inputFileName = '/home/rama/Downloads/lipsum.txt';
  readFileAsync(inputFileName)
    .then((data) => {

      const uppercaseContent = convertToUpperCase(data);
      const newUppercaseFileName = 'uppercase.txt';
      return writeFileAsync(newUppercaseFileName, uppercaseContent)
        .then(() => {
          console.log(newUppercaseFileName);


          const lowercaseAndSplitContent = convertToLowerCaseAndSplit(uppercaseContent);
          const newLowercaseSplitFileName = 'lowercase_split.txt';
          return writeFileAsync(newLowercaseSplitFileName, lowercaseAndSplitContent.join('\n'))
            .then(() => {
              console.log(newLowercaseSplitFileName);


              const filenames = [newUppercaseFileName, newLowercaseSplitFileName];
              const sortedContent = filenames
                .map((filename) => fs.readFile(filename, 'utf8'))
                .sort()
                .join('\n');
              const newSortedFileName = 'sorted.txt';
              return writeFileAsync(newSortedFileName, sortedContent)
                .then(() => {
                  console.log(newSortedFileName);


                  const filenamesContent = filenames.join('\n');
                  const filenamesFileName = 'filenames.txt';
                  return writeFileAsync(filenamesFileName, filenamesContent)
                    .then(() => {
                      console.log(filenamesFileName);


                      return readFileAsync(filenamesFileName)
                        .then((dataToDelete) => {
                          const filesToDelete = dataToDelete.split('\n').filter(Boolean);
                          const deletionPromises = filesToDelete.map((file) => fs.unlink(file)
                            .then(() => console.log('File deleted:', file))
                            .catch((err) => console.error(err))
                          );
                          return Promise.all(deletionPromises);
                        })
                        .catch((err) => console.error(err));
                    })
                    .catch((err) => console.error(err));
                })
                .catch((err) => console.error(err));
            })
            .catch((err) => console.error(err));
        })
        .catch((err) => console.error(err));
    })
    .catch((err) => console.error(err));

}

module.exports = fsProblem2;
